# x est le tensor de la valeur de chacun des output du dernier layer du model
import torch
from torch.autograd import Variable
import torch.utils.data
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')  # Not working with GPU for now
# device = torch.device('cpu')
print(device)


# convert the dataset to a pytorch tensor
train_x = torch.Tensor(np.stack(train_cnn1))


class Net(nn.Module):
    def __init__(self, nb_var, nb_actions, time_window):
        """
        Neural net class
        :param nb_var: nomber of variables ie number of 'images' for one observation (similar to RGB)
        :param nb_actions: number of actions in the portfolio
        :param time_window: len of the historic
        """
        super(Net, self).__init__()
        self.time_window = time_window
        self.nb_actions = nb_actions
        # maybe add a conv here to make the tensor more square
        self.bn0 = nn.BatchNorm2d(5)
        self.conv1 = nn.Conv2d(nb_var, 5, (5, 5))
        self.bn1 = nn.BatchNorm2d(5)
        self.conv2 = nn.Conv2d(5, 2, (3, 5))  # ((W-s +2P)/S) +1
        self.bn2 = nn.BatchNorm2d(2)
        self.dropout1 = nn.Dropout(0.5)
        self.fc1 = nn.Linear(33 * 4 * 2, 50)
        self.dropout2 = nn.Dropout(0.5)
        self.fc2 = nn.Linear(50, self.nb_actions)

    def forward(self, x):
        """
        forward method
        :param x: input tensor
        :return:
        """
        x = self.bn0(x)
        # x = F.relu(self.conv1(x))
        # x = F.relu(self.conv2(x))
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = x.view(-1, 33 * 9 * 2)
        x = F.relu(self.fc1(self.dropout1(x)))
        x = F.softmax(self.fc2(self.dropout2(x)), dim=1)
        return x

    def train_gd(self, train_x, future_prices, epochs, valid_sets, lr=0.0001, batch_size=42):
        """
        training method with Gradient descent
        :param train_x: input tensor
        :param future_prices: list of np array with future prices
        :param epochs: nb of epochs
        :param lr: learning rate
        :param batch_size: the batch sixe MUST BE ONE TO BE COMPATIBLE WITH THE ACTUAL LOSS FUNCTION
        :return:
        """
        # init the optimizer TBD put optimizer as an option in the method
        optimizer = optim.Adam(self.parameters(), lr=lr)  # optim.SGD(self.parameters(), lr=lr, momentum=0.9)
        # Repeat n times (n= epochs)
        for epoch in range(epochs):
            # init running loss for the epoch
            running_loss = 0.0
            # For each observation of the dataset (ie mini batchs of size 1)
            for i in range(train_x.shape[
                               0] // batch_size):  # WARNING input number of observations should be a multiple of the batch size
                # create observations for the mini batch
                batch_x, batch_y = train_x[i * batch_size: (i + 1) * batch_size].to(device), future_prices[
                                                                                             i * batch_size: (
                                                                                                                         i + 1) * batch_size]
                batch_y = torch.Tensor(batch_y).to(device)
                # empty the optimizer 'memory'
                optimizer.zero_grad()
                # pass the input through the net ie forward
                output = self(batch_x)
                # compute loss
                # loss = period_return(output, batch_y)
                # loss = sharp_loss(output, batch_y, risk_free_rate=0.0)
                # print(sharpe(output, batch_y, risk_free_rate=0.0))
                loss = sharpe(output, batch_y, risk_free_rate=0.0).sum()
                if torch.isnan(loss):
                    break
                if loss.data == 0:
                    break
                # retropropagate the gradient
                loss.backward()
                optimizer.step()
                # print statistics
                running_loss += loss.item()
                # if i % 365 == 364:  # print every 2000 mini-batches
                #    print('[%d, %5d] loss: %.3f' %
                #          (epoch + 1, i + 1, running_loss / i))


            valid_loss = sharpe(self.forward(torch.FloatTensor(np.stack(valid_sets[0])).to(device)),
                                torch.FloatTensor(valid_sets[1]).to(device)).mean()
            if epoch % 100 == 0:
                print('average loss for epoch {}: '.format(epoch), running_loss / (train_x.shape[0] // batch_size),
                      ' average validation loss:', valid_loss.cpu().detach().item())
        print('finish training')


net = Net(nb_var=5, nb_actions=10, time_window=41)
net.to(device)
net.train_gd(train_x=train_x, future_prices=train_next_prices, epochs=50000, lr=0.00001, batch_size=33, valid_sets=[valid_cnn1, valid_next_prices])

pred = net.forward(train_x.to(device))
pred_pd = pd.DataFrame(pred.cpu().detach().numpy())

names = stocks.sort()
colors = ['#1F2041', '#4B3F72', '#FFC857', '#00BFB3', '#19647E', '#F1DEDE', '#D496A7', '#5D576B', '#6CD4FF', '#FE938C']#,'#2E86AB', '#A23B72', '#F18F01', '#C73E1D', '#3B1F2B']
plt.figure()
for i in range(pred_pd.shape[1]):
    plt.plot(pred_pd.loc[:, i], label=stocks[i], color=colors[i])
    plt.legend()

