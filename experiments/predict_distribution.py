import tensorflow as tf
import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout, Masking
from keras.models import Model, Input
from keras.callbacks import History, ModelCheckpoint, TerminateOnNaN, EarlyStopping
from scipy import stats


class neural_net():
    def __init__(self):
        # self.train= train
        # self.mean_return= mean_return
        # self.std= std
        self.init_callbacks()
        self.init_net()

    def init_callbacks(self):
        self.history = History()
        self.nanterminator = TerminateOnNaN()
        self.filepath = "D:\madoff_distri_mae.hdf5"
        # early stop
        self.early_stop = EarlyStopping(monitor='val_loss', min_delta=0.0001, patience=50, verbose=1, mode='min')
        # pannes bin
        self.checkpoint = ModelCheckpoint(self.filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='min')

    def init_net(self):
        input_layer = Input(shape=(train.shape[1], 1), name='input_layer')
        mask= Masking(mask_value=-99)(input_layer)
        lstm = LSTM(units=50, return_sequences=True ,name='lstm_layer')(mask)
        dropout= Dropout(0.5)(lstm)
        lstm2 = LSTM(units=50, name='lstm_layer2')(dropout)
        dropout2 = Dropout(0.5)(lstm2)
        dense_mean_return = Dense(units=1, activation='linear', name='output_mean_return')(dropout2)
        dense_std = Dense(units=1, activation='linear', name='output_std')(dropout2)

        self.net = Model(inputs=input_layer, outputs=[dense_mean_return, dense_std])
        self.net.compile(optimizer='adam', loss='mean_absolute_error', loss_weights=[1., 1.], metrics=['mae'])

    def train(self, train_set, train_y, epochs, batch_size, valid_set, valid_y):
        self.net.fit(train_set
                     , train_y
                     , epochs=epochs
                     , batch_size=batch_size
                     , validation_data=(valid_set, valid_y)
                     , shuffle=True
                     , callbacks=[self.nanterminator, self.history, self.checkpoint, self.early_stop]
                     , verbose= 2)


# TBD: add input with ticker info
# CHANGE LOSS TO MAE
# change lr
NN = neural_net()
NN.net.summary()
NN.train(train, [train_mean_return, train_std], epochs=200, batch_size=256, valid_set= valid, valid_y= [valid_monthly_return, valid_std])


