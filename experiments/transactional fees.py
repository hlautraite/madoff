import load_transform
import losses
import quandl
import torch
from torch.autograd import Variable
import torch.utils.data
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#from plot_results import plot_results, get_results

PREDICT_LENGHT= 10


########################################################
# LOAD DATA                                            #
########################################################

quandl.ApiConfig.api_key = '4mjpNRRxgeW47CmQiJ7m'

stocks = ['AAPL', 'AMZN', 'GOOGL', 'MSFT', 'NVDA']#,'IBM', 'INTC', 'ATVI', 'ADBE', 'BIDU']#, 'CSCO', 'CTXS', 'EA', 'EXPE', 'NFLX', 'UBI.PA']
data= load_transform.load_data(stock_list=stocks)
#data = load_transform.get_data(stocks, '2000-01-01', '2019-01-17')

#change data types
data['Date']= pd.to_datetime(data['Date'])

#add new variable to compute the difference between the highest and lowest price of the day
data['amplitude']= data['High']- data['Low']
censored_data = load_transform.trim_data(data)
train = load_transform.get_time_window(censored_data, -1, '2000-01-01', '2017-01-01')
valid = load_transform.get_time_window(censored_data, -1, '2017-01-01', '2019-03-24')
#valid= valid.loc[valid['date']!= '2017-08-07', :] # problem in the data for this particular date

gen_cnn = load_transform.data_generator_cnn(data= train, date_col= 'Date', stock_col='ticker', x_cols=['High', 'Low', 'Adj Close', 'Volume','amplitude'])
gen_cnn.scale_data()
scaler= gen_cnn.get_scaler()
gen_cnn.lag_data(50)
train_cnn=gen_cnn.get_created_images()
train_cnn1= train_cnn[:-PREDICT_LENGHT] # a changer!! mettre dans la classe
train_next_prices= gen_cnn.get_next_prices(PREDICT_LENGHT, 'Adj Close')
train_next_prices= train_next_prices[:-PREDICT_LENGHT]

gen_cnn_test = load_transform.data_generator_cnn(data= valid, date_col= 'Date', stock_col='ticker', x_cols=['High', 'Low', 'Adj Close', 'Volume','amplitude'])
gen_cnn_test.scale_data(scaler=scaler)
gen_cnn_test.lag_data(50)
valid_cnn=gen_cnn_test.get_created_images()
valid_cnn1= valid_cnn[:-PREDICT_LENGHT]
valid_next_prices= gen_cnn_test.get_next_prices(PREDICT_LENGHT, 'Adj Close')
valid_next_prices= valid_next_prices[:-PREDICT_LENGHT]



################################################################
# MODELISATION                                                 #
################################################################

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')  # Not working with GPU for now
train_x = torch.Tensor(np.stack(train_cnn1))
valid_x=torch.FloatTensor(np.stack(valid_cnn1)).to(device)

sharpe= losses.sharpe

def plot_results(pred, next_prices):
    nb_shares= pred/torch.cuda.FloatTensor(next_prices).to(device)[:,0,:]
    values=[1000]
    for i in range(1,pred.size()[0]):
        nb_shares= (pred[i-1,:]*values[i-1])/torch.cuda.FloatTensor(next_prices).to(device)[i-1,0,:]
        values.append(np.sum(nb_shares.cpu().detach().numpy()*np.array(next_prices[i][0])))
    plt.plot(pd.Series(values))
    plt.show()
    print('final portfolio value: ', values[-1])

def get_results(pred, next_prices):
    nb_shares= pred/torch.cuda.FloatTensor(next_prices).to(device)[:,0,:]
    values=[1000]
    for i in range(1,pred.size()[0]):
        nb_shares= (pred[i-1,:]*values[i-1])/torch.cuda.FloatTensor(next_prices).to(device)[i-1,0,:]
        values.append(np.sum(nb_shares.cpu().detach().numpy()*np.array(next_prices[i][0])))
    print("final portfolio value: ", values[-1])
    return values


class Net(nn.Module):
    def __init__(self, nb_var, nb_actions, time_window):
        """
        Neural net class
        :param nb_var: nomber of variables ie number of 'images' for one observation (similar to RGB)
        :param nb_actions: number of actions in the portfolio
        :param time_window: len of the historic
        """
        super(Net, self).__init__()
        self.time_window = time_window
        self.nb_actions = nb_actions
        # maybe add a conv here to make the tensor more square
        self.bn0 = nn.BatchNorm2d(5)
        self.conv1 = nn.Conv2d(nb_var, 2, (1, 3))
        self.bn1 = nn.BatchNorm2d(2)
        self.conv2 = nn.Conv2d(2, 20, (1, 49))  # ((W-s +2P)/S) +1
        self.bn2 = nn.BatchNorm2d(20)
        self.dropout1 = nn.Dropout(0.5)
        self.conv3 = nn.Conv2d(20, 1, 1)
        # self.fc1 = nn.Linear(5, 5)
        # self.dropout2 = nn.Dropout(0.5)
        # self.fc2 = nn.Linear(50, self.nb_actions)

    def forward(self, x):
        """
        forward method
        :param x: input tensor
        :return:
        """
        x = self.bn0(x)
        # x = F.relu(self.conv1(x))
        # x = F.relu(self.conv2(x))
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.conv3(x))
        x = F.softmax(x.view(-1, 5), dim=1)
        # x = F.relu(self.fc1(self.dropout1(x)))
        # x = F.softmax(self.fc2(self.dropout2(x)), dim=1)
        return x

    def train_gd(self, train_x, future_prices, epochs, valid_sets, lr=0.0001, batch_size=42):
        """
        training method with Gradient descent
        :param train_x: input tensor
        :param future_prices: list of np array with future prices
        :param epochs: nb of epochs
        :param lr: learning rate
        :param batch_size: the batch sixe MUST BE ONE TO BE COMPATIBLE WITH THE ACTUAL LOSS FUNCTION
        :return:
        """
        # init the optimizer TBD put optimizer as an option in the method
        optimizer = optim.Adam(self.parameters(), lr=lr)  # optim.SGD(self.parameters(), lr=lr, momentum=0.9)
        # Repeat n times (n= epochs)
        for epoch in range(epochs):
            # init running loss for the epoch
            running_loss = 0.0
            previous_stock_qty= torch.zeros((1,self.nb_actions)).to(device)
            # For each observation of the dataset (ie mini batchs of size 1)
            for i in range(train_x.shape[0] // batch_size):  # WARNING input number of observations should be a multiple of the batch size
                # create observations for the mini batch
                batch_x, batch_y = train_x[i * batch_size: (i + 1) * batch_size].to(device), future_prices[i * batch_size: (i + 1) * batch_size]
                batch_y = torch.Tensor(batch_y).to(device)
                # empty the optimizer 'memory'
                optimizer.zero_grad()
                # pass the input through the net ie forward
                output = self(batch_x)
                # compute loss
                st_q, cash = losses.get_entier_values(output, batch_y)
                self.st_q= st_q
                self.previous_stock_qty=previous_stock_qty
                self.losses = losses.sharpe_entier(st_q, cash, batch_y, previous_stock_qty, trading_fees=0.00326541)
                previous_stock_qty= st_q[-1].view((1,self.nb_actions)).detach()
                self.loss= self.losses.mean()
                if torch.isnan(self.loss):
                    break
                if self.loss.item == 0.:
                    break
                # retropropagate the gradient
                self.loss.backward()
                optimizer.step()
                # print statistics
                running_loss += self.loss.item()
                # if i % 365 == 364:  # print every 2000 mini-batches
                #    print('[%d, %5d] loss: %.3f' %
                #          (epoch + 1, i + 1, running_loss / i))

            if epoch % 100 == 0:
                val_q, val_cash=losses.get_entier_values(self.forward(torch.FloatTensor(np.stack(valid_sets[0])).to(device)), torch.FloatTensor(valid_sets[1]).to(device), value_1000=1.19)
                valid_loss = losses.sharpe_entier(val_q, val_cash,torch.FloatTensor(valid_sets[1]).to(device),torch.zeros((1,self.nb_actions)).to(device)).mean()  # / period_return(self.forward(torch.FloatTensor(np.stack(valid_sets[0])).to(device)), torch.FloatTensor(valid_sets[1]).to(device)).std()
                print('average loss for epoch {}: '.format(epoch), running_loss / (train_x.shape[0] // batch_size),
                      ' average validation loss:', valid_loss.cpu().detach().item())
            #if epoch % 1000 == 0:
                #plot_results(self(valid_x.to(device)), valid_next_prices)
        print('finish training')


net = Net(nb_var=5, nb_actions=5, time_window=51)
net.to(device)
net.train_gd(train_x=train_x, future_prices=train_next_prices, epochs=30000, lr=0.001, batch_size=133, valid_sets=[valid_cnn1, valid_next_prices])
net.train_gd(train_x=train_x, future_prices=train_next_prices, epochs=20000, lr=0.0001, batch_size=133, valid_sets=[valid_cnn1, valid_next_prices])

#net.train_gd(train_x=train_x, future_prices=train_next_prices, epochs=1000, lr=0.001, batch_size=133, valid_sets=[valid_cnn1, valid_next_prices])

#plt.figure()
#plot_results(net(train_x.to(device)), train_next_prices)
plot_results(net(valid_x.to(device)), valid_next_prices)

#torch.save(net.state_dict(), 'd:/pytorch_model_fees')
def plot_stocks_repartition_pct(net, dataset):
    pred = net.forward(torch.FloatTensor(np.stack(dataset)).to(device))
    pred_pd= pd.DataFrame(pred.cpu().detach().numpy())
    stocks.sort()
    for i in range(pred_pd.shape[1]):
        plt.plot(pred_pd.iloc[:,i], label= stocks[i])
    plt.legend()
    plt.show()
plot_stocks_repartition_pct(net, valid_cnn1[-500:])