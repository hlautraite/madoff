import matplotlib.pyplot as plt
import pandas as pd

#######################################
#Graphs for nn agent                  #
#######################################

def plot_stocks_repartition_pct(net, dataset):
    pred = net.forward(torch.FloatTensor(np.stack(valid_cnn1)).to(device))
    pred_pd= pd.DataFrame(pred.cpu().detach().numpy())
    stocks.sort()
    for i in range(pred_pd.shape[1]):
        plt.plot(pred_pd.iloc[:,i], label= stocks[i])
    plt.legend()
    plt.show()

def plot_results(pred, next_prices):
    nb_shares= pred/torch.cuda.FloatTensor(next_prices).to(device)[:,0,:]
    values=[1000]
    for i in range(1,pred.size()[0]):
        nb_shares= (pred[i-1,:]*values[i-1])/torch.cuda.FloatTensor(next_prices).to(device)[i-1,0,:]
        values.append(np.sum(nb_shares.cpu().detach().numpy()*np.array(next_prices[i][0])))
    plt.plot(pd.Series(values))
    plt.show()
    print('final portfolio value: ', values[-1])

def get_results(pred, next_prices):
    nb_shares= pred/torch.cuda.FloatTensor(next_prices).to(device)[:,0,:]
    values=[1000]
    for i in range(1,pred.size()[0]):
        nb_shares= (pred[i-1,:]*values[i-1])/torch.cuda.FloatTensor(next_prices).to(device)[i-1,0,:]
        values.append(np.sum(nb_shares.cpu().detach().numpy()*np.array(next_prices[i][0])))
    print("final portfolio value: ", values[-1])
    return values



plt.plot(NN.history.history['loss'],    label='training')
plt.plot(NN.history.history['val_loss'],label='validation')

NN.net.load_weights('D:\madoff_distri_mae.hdf5')

predict_mean_return, predict_std= NN.net.predict(valid)
results= pd.DataFrame({'predict_mean_return': predict_mean_return.flatten(),
                       'mean_return': valid_monthly_return,
                       'predict_std': predict_std.flatten(),
                       'std': valid_std})

#plot mean daily return
for i in range(len(stock_duration)):
    if i==0:
        plt.figure()
        plt.plot(results['mean_return'][0:stock_duration[i]], label='true return')
        plt.plot(results['predict_mean_return'][0:stock_duration[i]], label='predicted_return')
        plt.title(stocks[i])
        plt.legend()
    else:
        plt.figure()
        plt.plot(results['mean_return'][sum(stock_duration[:i]): sum(stock_duration[:i])+ stock_duration[i]], label='true return')
        plt.plot(results['predict_mean_return'][sum(stock_duration[:i]): sum(stock_duration[:i])+ stock_duration[i]], label='predicted_return')
        plt.title(stocks[i])
        plt.legend()

#plot std daily return
for i in range(len(stock_duration)):
    if i==0:
        plt.figure()
        plt.plot(results['std'][0:stock_duration[i]], label='true return')
        plt.plot(results['predict_std'][0:stock_duration[i]], label='predicted_return')
        plt.title(stocks[i])
        plt.legend()
    else:
        plt.figure()
        plt.plot(results['std'][sum(stock_duration[:i]): sum(stock_duration[:i])+ stock_duration[i]], label='true return')
        plt.plot(results['predict_std'][sum(stock_duration[:i]): sum(stock_duration[:i])+ stock_duration[i]], label='predicted_return')
        plt.title(stocks[i])
        plt.legend()


n = np.random.randint(0,300)
print(n)
bins = np.linspace(-1, 1, 100)
plt.hist(np.random.normal(results['mean_return'][n], results['std'][n]), bins, label='true dist')
plt.hist(np.random.normal(results['predict_mean_return'][n], results['predict_std'][n]), bins, label= 'predict_dist')
plt.legend()

results.to_csv("D:\madoff\daily_mean_return_distribution_pred.csv")





#######################
# Some more graphs    #
#######################

#df = censored_data.set_index('date')

def plot_stock_price(data):
    df = data[['Date', 'ticker', 'High']].set_index('Date')
    table = df.pivot(columns='ticker')
    # By specifying col[1] in below list comprehension
    # You can select the stock names under multi-level column
    table.columns = [col[1] for col in table.columns]

    plt.figure(figsize=(14, 7))
    for c in table.columns.values:
        plt.plot(table.index, table[c], lw=3, alpha=0.8,label=c)
    plt.legend(loc='upper left', fontsize=12)
    plt.ylabel('price in $')

plot_stock_price(gen_cnn.data_lag)


#inspect train
train_predict_return, train_predict_std = NN.net.predict(train)

plt.plot(train_monthly_return, label='true return')
plt.plot(train_predict_return **6, label= 'predicted_return')
plt.legend()

#plot std daily return
plt.plot(train_std, label='true std')
plt.plot(train_predict_std, label= 'predicted std')
plt.legend()


######################################
# visualize pytorch net              #
######################################

from graphviz import Digraph
import torch
from torch.autograd import Variable


def make_dot(var, params):
    """ Produces Graphviz representation of PyTorch autograd graph

    Blue nodes are the Variables that require grad, orange are Tensors
    saved for backward in torch.autograd.Function

    Args:
        var: output Variable
        params: dict of (name, Variable) to add names to node that
            require grad (TODO: make optional)
    """
    param_map = {id(v): k for k, v in params.items()}
    print(param_map)

    node_attr = dict(style='filled',
                     shape='box',
                     align='left',
                     fontsize='12',
                     ranksep='0.1',
                     height='0.2')
    dot = Digraph(node_attr=node_attr, graph_attr=dict(size="12,12"))
    seen = set()

    def size_to_str(size):
        return '(' + (', ').join(['%d' % v for v in size]) + ')'

    def add_nodes(var):
        if var not in seen:
            if torch.is_tensor(var):
                dot.node(str(id(var)), size_to_str(var.size()), fillcolor='orange')
            elif hasattr(var, 'variable'):
                u = var.variable
                node_name = '%s\n %s' % (param_map.get(id(u)), size_to_str(u.size()))
                dot.node(str(id(var)), node_name, fillcolor='lightblue')
            else:
                dot.node(str(id(var)), str(type(var).__name__))
            seen.add(var)
            if hasattr(var, 'next_functions'):
                for u in var.next_functions:
                    if u[0] is not None:
                        dot.edge(str(id(u[0])), str(id(var)))
                        add_nodes(u[0])
            if hasattr(var, 'saved_tensors'):
                for t in var.saved_tensors:
                    dot.edge(str(id(t)), str(id(var)))
                    add_nodes(t)

    add_nodes(var.grad_fn)
    return dot


"""
inputs = train_x[0:1]
y = Variable(net(Variable(inputs)), requires_grad=True)
st_value= (w*st_price).sum()
end_value= (w*end_price).sum()
out= -1* (end_value- st_value)/ st_value
print(out)
"""
g = make_dot(loss, net.state_dict())
g.view()

def main():
    print('import graph module')

if __name__=='__main__':
    main()
