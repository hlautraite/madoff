# Madoff

#### Deep Convolutional Direct Reinforcement Learning for portfolio management
 
 
 
                               Hadrien Lautraite                           	          Thomas Thiery
                          Dev Lead Data Science             	             Department of Psychology
                    Banque Nationale Du Canada                                 Université de Montréal
                        hadrien.lautraite@bnc.ca                             thomas.thiery@umontreal.ca

  
 
Abstract
In the past few years, reinforcement learning has shown success in a variety of domains. Notbly, it has achieved impressive performances in various games like chess, Go, Atari, and has even defeated a pro Starcraft player, showing its ability to perform extremely well even in non-discrete real-time video games. Reinforcement learning applications are not restricted to video games,  and can also be used for more practical applications such as optimizing stock allocation in financial portfolios. Optimizing portfolios’ return has been extensively studied using linear and bayesian methods (Konno et al., 1993), but this problem has only been partially studied with approaches inspired from new developments in deep learning algorithms. Here, we propose to use direct reinforcement learning - feeding a neural network with information about the past state of the market (close price, volume, high price, low price, ...) -  for a specific set of stocks and train it to maximize financial measures for a future time-frame. We optimize the return, the Sharpe ratio and the derivative Sharpe ratio (Moody et al., 2001) and compared our results with other Reinforcement learning methods (Recurrent Reinforcement Learning, Q-learning and TD-learning). We find that the derivative Sharpe ratio leads to the best results by maximizing the trade-off between risks and returns on investment. 

 
#Keywords:  Reinforcement learning, deep learning, portfolio management, CNN







###1	Problem context: Portfolio management
The portfolio management problem consists of selecting financial assets in order to diversify the total portfolio risk (Markowitz, 1991). In a nutshell, the principal goal of portfolio management is to select the best set of stocks in order to maximize returns while minimizing the risks. In our project, we consider a simpler version of the general portfolio management problem in the form of an asset allocation problem: Given a set of financial assets, which combination of these financial assets will lead to the best performance (i.e, optimizing the risk/returns tradeoffs) ?
The absolute value of returns can be computed by taking the value difference between the start and the end of a given time period (e.g., a day). It can be expressed in percentage by dividing the absolute return by the portfolio’s value at the beginning of the time period.
art = pt – pt-1
rt= r_t=(ar_t)/p_(t-1) 
with art is the absolute return for a period between period t-1 and t, pt the portfolio value at period t and pt-1 the portfolio value at time t-1, rt the period return expressed in percentage.
Risks can be measured by the volatility of the portfolio, which is similar to the standard deviation of the portfolio’s returns. In our research, we measure the standard deviation of daily returns for a specific time period. 
σ_t= √(∑_1^n▒〖(p_t- μ)〗^2 )
With σ_t the standard deviation of portfolio return between period t-1 and t and μ the mean daily return of the portfolio for a specific period.
The asset allocation problem is a multi-objective optimization problem. There is not a single best solution. However, some solutions are better than others, meaning that for the same level of risk, those solutions yield better returns. This was first introduced by Harry Markowitz with the notion of the efficient frontier (1952) : A portfolio is considered to be on the efficient frontier if, for the same level of risk, there is no portfolio with higher returns. 
In order to take into account this tradeoff between returns and risks, we use the Sharpe ratio (Sharpe 1966), that computes the excess returns (compared to the risk-free rate of return) per units of risk (deviation). It can be expressed as:
S= (μ-r^f)/σ
With S being the Sharpe ratio for a period and rf the risk-free rate for the given time period

 
Portfolio’s returns, standard deviations and Sharpe ratios for 250000 simulated portfolios

###2	Methods : Deep Convolutional Direct Reinforcement Learning
##2.1 Method and related work
There are many ways to approach the reinforcement learning problem for portfolio management. One would be to use a classical dynamic programming algorithm such as TD Learning or Q learning [1], [2]. Here, we propose to use direct reinforcement learning methods. We feed the network with information about the past state of the market (close price, volume, high price, low price, ...) for a specific set of stocks. The network outputs a list of weights that correspond to the weights every stock represented in the portfolio. We use a softmax activation function on the last layer to ensure that the sum of weights is equal to 1. 
This approach offers a few advantages over value function methods. First, it allows a simpler representation of the problem, since the model output is directly linked to the action space. Moreover, there is no need to discretize the action space and evaluate the value function for every state. Thus, we can avoid the Bellman's curse of dimensionality which is a common problem when using Q learning for the allocation of assets in portfolio management problems. Finally, direct reinforcement learning techniques seem to lead to more stable results [3]. This could be explained by the fact that noise and non-stationarity of the stock prices could be a serious problem for a value function approach [4].
Direct reinforcement learning has already being used and has shown very promising results for a number of applications. Specifically, Moody et al. [6] used recurrent reinforcement learning algorithms to maximize measures related to the Sharpe ratio. In their paper, they used an online approach where outputs from the last timestamp are directly fed as inputs to the next timestamp. We chose to adopt a more offline approach; instead of training the network to constantly adapt to the weights in order to maximize the portfolio Sharpe ratio for past n trading days, we train the network in order to find the weights that will maximize the Sharpe ratio for the next n trading days (without modifying the weights during this period). This allows our method to be suitable for more casual investors that wouldn’t want to make trading actions on a daily basis. We study the impact of different periods and techniques to re-allocate the stocks’ weights in the portfolio.
Lately, the use of deep convolutional neural networks for time series has led to very promising results by exceeding recurrent neural networks such as Gated Recurrent Unit [7] or Long Short Term Memory [8]. Specifically, Jiang et al. [4]¸ have shown very interesting results with CNNs outperforming approaches such LSTMs or other RNNs. 
##2.2 Network architecture
We use a multi-channel CNN, which is fed with historical information about stock prices. Each canal represents a feature describing the financial assets: adjusted close price, adjusted highest price during training period, adjusted lowest price during training period, the difference between highest and lowest adjusted prices and the exchange volume. For each trading day, we produce 5 (number of channels) matrices of m x n dimensions where m represents the number of stocks in the portfolio and n the number of previous trading days used as input. Thus every matrix represents the historical information for every stocks in the portfolio during the last n trading days for a particular feature. The network is composed of a series of convolutions followed by two fully connected layers. Finally, the output layer contains m neurons with a softmax activation function. 
We used several financial metrics as loss function and stochastic gradient descent in order to maximize the loss (or minimize the inverse of the loss): the Sharpe ratio and the derivative Sharpe ratio computed on data of the next h periods (usually one month). 
The derivative Sharpe ratio led to the most interesting results. Portfolios’ returns used as the loss function also provides interesting results, yielding higher returns but at the price of higher risks (higher standard deviation of portfolios’ returns).




###3	 References

[1] B. Van Roy, “Temporal-difference learning and applications in finance,” in Computational Finance 1999, Y. S. Abu-Mostafa, B. LeBaron, A. W. Lo, and A. S. Weigend, Eds. Cambridge, MA: MIT Press, 2001, pp. 447–461. 
[2] R. Neuneier, “Optimal asset allocation using adaptive dynamic programming,” in Advances in Neural Information Processing Systems, D. S. Touretzky, M. C. Mozer, and M. E. Hasselmo, Eds. Cambridge, MA: MIT Press, 1996, vol. 8, pp. 952–958.
[3] Xin Du, Jinjian Zhai, Koupin Lv, “Algorithm Trading using Q-Learning and Recurrent Reinforcement Learning”,  2016
[4] David W. Lu, “Agent Inspired Trading Using Recurrent Reinforcement Learning and LSTM Neural Networks”, 2017
[5] Zhengyao Jiang, Dixing Xu, Jinjun Liang, “A Deep Reinforcement Learning Framework for the Financial Portfolio Management Problem”, 2017
[6] J. Moody, M. Saffell, “Learning to Trade via Direct Reinforcement”, IEEE Transactions on Neural Networks, Vol.12, July, 2001
[7] Cho, Kyunghyun, Van Merrienboer, Bart, Gulcehre, Caglar, Bougares, Fethi, Schwenk, Holger, and Bengio, Yoshua. ,”Learning phrase representations using rnn encoder-decoder for statistical machine translation”, 2014
[8] Sepp Hochreiter and Jürgen Schmidhuber, “Long Short-Term Memory”, Neural Computation
Volume 9 | Issue 8 | November 15, 1997 p.1735-1780
