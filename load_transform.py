import pandas as pd
import numpy as np
#import matplotlib.pyplot as plt
import quandl
#import scipy.optimize as sco
import datetime
from sklearn.preprocessing import MinMaxScaler
import sklearn

# We use the Quandle api to get the data
quandl.ApiConfig.api_key = '4mjpNRRxgeW47CmQiJ7m'

#list of stocks we want to use
#stocks = ['AAPL', 'AMZN', 'GOOGL', 'MSFT', 'NVDA','IBM', 'INTC', 'ATVI', 'ADBE', 'BIDU']#, 'CSCO', 'CTXS', 'EA', 'EXPE', 'NFLX', 'UBI.PA']

#call quandl API to get the data
def load_data(stock_list, path='D:/madoff2/stocks_data'):
    for i in range(len(stock_list)):
        if i==0:
            data= pd.read_csv("{}/{}.csv".format(path,stock_list[i]))
            data['ticker']= stock_list[i]
        else:
            df= pd.read_csv("{}/{}.csv".format(path,stock_list[i]))
            df['ticker']= stock_list[i]
            data= data.append(df, ignore_index=True)
    return data

def get_data_online(stock_list, strt_date, end_date):
    data = quandl.get_table('WIKI/PRICES', ticker=stock_list,
                            qopts={'columns': ['ticker', 'date', 'adj_high', 'adj_low', 'adj_close', 'adj_volume']},
                            date={'gte': strt_date, 'lte': end_date}, paginate=True)
    return data

#data = get_data(stocks, '2000-01-01', '2019-01-17')

#add new variable to compute the difference between the highest and lowest price of the day
#data['amplitude']= data['adj_high']- data['adj_low']

# function to trim the data new starting date is the starting date of the stock most recently introduced into the market
def trim_data(data):
    max_strt_date = max(data.groupby(['ticker']).min()['Date'])
    censored_data = data.loc[data['Date'] > max_strt_date, :]
    return censored_data


#censored_data = trim_data(data)

# function to get the dataset between two dates
def get_time_window(data, ticker, start, end):
    """
    start & end: string dd-mm-yyyy
    """
    start = datetime.datetime.strptime(start, '%Y-%m-%d')
    end = datetime.datetime.strptime(end, '%Y-%m-%d')
    if ticker == -1:
        return data.loc[((start <= data['Date']) & (data['Date'] < end)), :]
    else:
        return data.loc[((data['ticker'] == ticker) & (start <= data['Date']) & (data['Date'] < end)), :]


#train = get_time_window(censored_data, -1, '2000-01-01', '2017-01-01')
#valid = get_time_window(censored_data, -1, '2017-01-01', '2019-01-21')
#valid= valid.loc[valid['date']!= '2017-08-07', :] # problem in the data for this particular date
#test = get_time_window(data, -1, '2017-08-01', '2019-01-20')


class data_generator_lstm():

    def __init__(self, stocks_list, data):
        """
        this class generate dataset to use a lstm in order to predict the future mean return and std of a stock
        :param stocks_list: list of the stocks
        :param data: dataset to transform
        """
        self.stocks = stocks_list
        self.data = data
        self.train = []
        self.mean_return = []
        self.std = []
        self.stocks_duration= []

    def create_dataset(self, pass_length, pred_length):
        """
        function to create the dataset, one observation by stock by day
         output is a 3d tensor to be fill to the RNN.
         new dataset will start at the past_lengh th observation of original dataset and will end at the len(dataset) - pred_lengh
        :param pass_length: number observations to look back
        :param pred_length: number of future observation to consider when trying to predict mean return and std
        :return:
        """
        for stock in self.stocks:
            df_stock = self.data.loc[self.data['ticker'] == stock, :]
            for i in range(pass_length, df_stock.shape[0] - pred_length):
                self.train.append(df_stock['adj_close'][i - pass_length:i].pct_change().tolist())
                self.mean_return.append((df_stock['adj_close'][i: i + pred_length].pct_change() +1).product())
                self.std.append(df_stock['adj_close'][i: i + pred_length].pct_change().std())
            self.stocks_duration.append(df_stock.shape[0]- pass_length - pred_length)
        self.train_list = self.train
        self.train_array = np.array(self.train_list)
        self.train_array[np.isnan(self.train)] = -99

        return np.reshape(self.train_array, (self.train_array.shape[0], self.train_array.shape[1], 1)), self.mean_return, self.std

    def get_stock_duration_list(self):
        """
        How much observations there is for each stock, will be used to construct graphs
        :return: stock duration list
        """
        return self.stocks_duration

#gen = data_generator_lstm(stocks, train)
#train, train_monthly_return, train_std = gen.create_dataset(180, 30)
#del gen
#gen = data_generator_lstm(stocks, valid)
#valid, valid_monthly_return, valid_std = gen.create_dataset(180, 30)
#stock_duration= gen.get_stock_duration_list()

class data_generator_cnn():
    def __init__(self, data, date_col, stock_col, x_cols):
        """
        class to generate dataset to be use by the cnn for the nn agent.
        Each observations is compose of n 'images', n being the number of variables in the dataset
        each image is a set of line,  one line by stock with the historic for a specific variable (n)
        new dataset will start at the past_length th observation of original dataset and will end at the len(dataset) - pred_length
        :param data: dataset
        :param date_col: STRING: name of the date column
        :param stock_col: STRING: name of the column indicating the stock (ticker)
        :param x_cols: LIST variables to be used for prediction
        """
        self.data = data
        self.date_col= date_col
        self.stock_col= stock_col
        self.x_cols= x_cols
        self.stocks_duration= []
        self.image_list= []
        #self.date_list= self.data[self.date_col].unique()
        self.next_prices= []
        self.stock_list= self.data[self.stock_col].unique()

    def scale_data(self, min=0, max=1, scaler = None):
        """
        Rescale data with MinMax Scaler
        :param min: minimum value for reshaping
        :param max: maximum value for reshaping
        :param scaler: scaler object to transform the data- to be used to scale the test set with the same scaler as the one of train
        :return:
        """
        num_var= []
        for col in self.data.columns:
            if self.data[col].dtypes == 'float64':
                num_var.append(col)
        self.data_sc= self.data
        if scaler== None:
            self.scaler = MinMaxScaler((min, max))
            self.scaler.fit(self.data_sc[num_var])
        else:
            self.scaler=scaler
        self.data_sc[num_var] = self.scaler.transform(self.data_sc[num_var])

    def get_scaler(self):
        """
        method to get scaler object
        :return:
        """
        return self.scaler

    def lag_data(self, pass_length):
        """
        method to lag the data, ie adding new column for each var with the observation at time t-1, t-2, ..., t-n
        :param pass_length: INT: pass length for lagging
        :return:
        """
        self.data_sc.reset_index(drop=True,inplace=True)
        self.data_sc.sort_values([self.date_col, self.stock_col], ascending=True, inplace=True)
        cols, names=[],[]
        features= self.x_cols.copy()
        features.insert(0,self.stock_col)
        data_features=self.data_sc.copy()[features]
        for i in range(pass_length, 0, -1):
            cols.append(data_features.shift(i*len(self.stock_list)))
            names += [('{}_t-{}'.format(features[j], i)) for j in range(len(features))]
        t_seri = pd.concat(cols, axis=1)
        t_seri.columns = names
        self.t_seri=t_seri
        df_final = pd.concat([self.data_sc.loc[:,(h for h in self.data_sc.columns if (h not in features) or h== self.stock_col)], t_seri], axis=1)
        for i in range(pass_length, 0, -1):
            df_final.loc[df_final[self.stock_col] != df_final['{}_t-{}'.format(self.stock_col, i)], (col for col in df_final.columns if '_t-{}'.format(i) in col)] = np.nan
            df_final = df_final.drop((col for col in df_final.columns if '{}_t-{}'.format(self.stock_col, i) in col), axis=1)
        features = features[1:]
        data_features = data_features[features]
        df_final = pd.concat([df_final, data_features], axis=1)
        df_final.dropna(inplace=True)
        self.data_lag= df_final

    def get_created_images(self):
        """
        method to create the images based on the lagged data
        :return:
        """
        self.data_lag.sort_values([self.date_col, self.stock_col], ascending=True, inplace=True)
        self.date_list= self.data_lag[self.date_col].unique()
        for d in self.date_list:
            this_image= []
            df= self.data_lag.loc[self.data_lag[self.date_col] == d, :]
            for col in self.x_cols:
                if col != self.stock_col:
                    this_image.append(df.loc[:,(x for x in df.columns if col in x)].values)
            self.image_list.append(this_image)
        return self.image_list

    def get_next_prices(self, next_prices_time_window, price_col= 'adj_close'):
      """
      method to get the adj_close price of every stock for the next period
      :param time_windows: number of days to consider for the time windows
      :param price_col: STRING the price column
      :return:
      """
      prices= self.data_lag[price_col].to_list()
      prices= np.reshape(prices, (self.data_lag.shape[0]//len(self.stock_list), len(self.stock_list)))
      print(prices.shape)
      self.next_prices=[]
      for i in range(self.data_lag.shape[0]//len(self.stock_list)):
          this_bach= prices[i:(i+1)+(next_prices_time_window)]
          self.next_prices.append(this_bach)
      return self.next_prices




"""
gen_cnn = data_generator_cnn(data= train, date_col= 'date', stock_col='ticker', x_cols=['adj_high', 'adj_low', 'adj_close', 'adj_volume','amplitude'])
gen_cnn.scale_data()
scaler= gen_cnn.get_scaler()
gen_cnn.lag_data(40)
train_cnn=gen_cnn.get_created_images()
train_cnn1= train_cnn[:-20] # a changer!! mettre dans la classe
train_next_prices= gen_cnn.get_next_prices(40, 20)
#train_next_prices= train_next_prices[:]

gen_cnn_test = data_generator_cnn(data= valid, date_col= 'date', stock_col='ticker', x_cols=['adj_high', 'adj_low', 'adj_close', 'adj_volume','amplitude'])
gen_cnn_test.scale_data()
gen_cnn_test.lag_data(40)
valid_cnn=gen_cnn_test.get_created_images()
valid_cnn1= valid_cnn[:-20]
valid_next_prices= gen_cnn_test.get_next_prices(40, 20)
#valid_next_prices= valid_next_prices[:-30]
"""

if __name__== "__main__":
    print("import")
