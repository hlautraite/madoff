 import pandas as pd
import numpy as np
import datetime
import load_transform
"""
A few test to verify the data generatir classes
"""

df_test= pd.DataFrame({'date': ['2018-01-01', '2018-01-02', '2018-01-03', '2018-01-04', '2018-01-05', '2018-01-06', '2018-01-07', '2018-01-08', '2018-01-09', '2018-01-10']*3,
                       'ticker': ['a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'c', 'c', 'c', 'c', 'c', 'c', 'c', 'c', 'c', 'c'],
                       'v1': [i for i in range(30)],
                       'v2': [i for i in range(100,130)],
                       'v3': [i for i in range(1000,1030)]})

df_test['date']= pd.to_datetime(df_test['date'])#, yearfirst=True)


gen_test = load_transform.data_generator_cnn(data= df_test, date_col= 'date', stock_col='ticker', x_cols=['v1', 'v2', 'v3'])
#gen_cnn.scale_data()
#scaler= gen_cnn.get_scaler()
gen_test.data_sc= gen_test.data
gen_test.lag_data(3)
gen_test.data_lag
train_cnn=gen_test.get_created_images()
train_cnn1= train_cnn[:-1]
train_next_prices= gen_test.get_next_prices(3, 1, 'v1')


