import torch

def period_return(w, price_next_month):
    """
    loss function for the trasining agent. It is the return in % of the portfolio with a specific set of weights
    :param w: TENSOR weights, ie the proportion of the portfolio to put in each stock
    :param price_next_month: np array : the prices of each stock at each day
    :return:
    """
    weights = w
    trgt_time_window = price_next_month.shape[1]
    batch_size = w.shape[0]
    nb_stocks = w.shape[1]
    stock_qty = w / price_next_month[:, 0, :]
    #stock_qty = stock_qty.view((batch_size, -1, nb_stocks)).expand((batch_size, trgt_time_window, nb_stocks))
    st_price = price_next_month[:, 0, :]
    end_price = price_next_month[:, trgt_time_window-1, :]
    st_value= torch.sum(stock_qty*st_price, dim=1)
    end_value= torch.sum(stock_qty* end_price, dim=1)
    cumul_return = -10 * ((end_value - st_value) / st_value)
    # cumul_return = Variable((end_value-st_value)/st_value, requires_grad=True)
    return cumul_return


# ATTENTION NE MARCHE QUE POUR BATCH DE 1
def sharp_loss(w, price_next_month, risk_free_rate=0.002):
    """
    loss function, the sharp ratio: (portfolio rate of return - risk free rate)/ std(portfolio returns)
    :param w: TENSOR: weights, ie the proportion of the portfolio to put in each stock
    :param price_next_month: np array : the prices of each stock at each day
    :param risk_free_rate: periodic risk free rate
    :return:
    """
    weights = w  # .to('cpu')
    st_price = torch.FloatTensor(price_next_month[0]).to(device)
    end_price = torch.FloatTensor(price_next_month[price_next_month.shape[0] - 1]).to(device)
    cumul_return = ((torch.sum(weights * end_price)) - (torch.sum(weights * st_price))) / torch.sum(
        (weights * st_price))
    daily_cumul_return = (1 + cumul_return.item()) ** (1 / (price_next_month.shape[0] - 1))
    new_price = torch.sum(st_price * w).item()
    mean_cumul_value = []
    for i in range(price_next_month.shape[0]):  # vraiment pas terrible, trouver un moyen de le faire sans loop
        mean_cumul_value.append(new_price)
        new_price = new_price * (daily_cumul_return)
    mean_cumul_value = torch.FloatTensor(mean_cumul_value)
    std = torch.sqrt(
        torch.sum((torch.matmul(torch.FloatTensor(price_next_month),
                                weights.t()) - mean_cumul_value) ** 2))  # a revoir mesure ecart de prix pas std
    sharp = -1 * (cumul_return - Variable(torch.FloatTensor([risk_free_rate]))) / torch.max(
        torch.FloatTensor([std, 0.005]))
    return sharp

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
def sharpe(w, price_next_month, risk_free_rate=0.002, device= device):
    trgt_time_window= price_next_month.shape[1]
    batch_size= w.shape[0]
    nb_stocks= w.shape[1]
    stock_qty= w / price_next_month[:,0,:]
    stock_qty= stock_qty.view((batch_size,-1,nb_stocks)).expand((batch_size, trgt_time_window, nb_stocks))
    portfolio_value= torch.sum(stock_qty*price_next_month, dim=2)
    returns= portfolio_value[:,1:] - torch.cat((torch.zeros(batch_size,1).to(device),portfolio_value),1)[:, 1:-1]
    mean_return= torch.mean(returns, dim=1)
    std= torch.std(returns, dim=1)
    sharpe= -1 * mean_return/std
    return sharpe

def get_entier_values(w, price_next_month, value_1000=1.19):
    stocks_value=  price_next_month[:, 0, :]
    stock_qty = torch.floor(w*value_1000 / stocks_value)
    cash = torch.sum(torch.fmod(w*value_1000, stocks_value), dim=1)
    return stock_qty, cash

def sharpe_entier(stock_qty, cash, price_next_month, previous_stock_qty, trading_fees=0.00326541, device= device):
    trgt_time_window= price_next_month.shape[1]
    batch_size= stock_qty.shape[0]
    nb_stocks= stock_qty.shape[1]
    previous_stock_qty= torch.cat((previous_stock_qty, stock_qty[:-1]),0)
    stock_dif = previous_stock_qty - stock_qty
    stock_dif = torch.where(stock_dif == 0., stock_dif, torch.tensor(1.).to(device))
    trading_cost = torch.sum(trading_fees * stock_dif, dim=1)
    stock_qty= stock_qty.view((batch_size,-1,nb_stocks)).expand((batch_size, trgt_time_window, nb_stocks))
    portfolio_value= torch.sum(stock_qty*price_next_month, dim=2)
    portfolio_value += cash.view((batch_size,1))
    portfolio_value[:,1:] -= trading_cost.view((batch_size,1))
    returns = portfolio_value[:, 1:]-torch.cat((torch.zeros(batch_size, 1).to(device), portfolio_value), 1)[:, 1:-1]
    mean_return = torch.mean(returns, dim=1)
    std = torch.std(returns, dim=1)
    sharpe = -1 * mean_return / std
    return sharpe

"""
w= torch.tensor([[.25,.75], [.23,.77]])
pr= torch.tensor([[[.5,.5],[.45,.55],[.47,.54]], [[.6,.6],[.65,.66],[.67,.70]]])
pr_st_1= [.0,.0]
st_q, cash= get_entier_values(w, pr)
s=sharpe_entier(st_q, cash, pr, previous_stock_qty= torch.tensor([[.0,.0]]), trading_fees=0.00326541)
print(s)
"""
